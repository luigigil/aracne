#ifndef SPIDER_H
#define SPIDER_H

#include <QDialog>
#include <QDebug>
#include "spiderworker.h"

namespace Ui {
class Spider;
}

class Spider : public QDialog {
    Q_OBJECT

signals:
    void set_mode(QString);

public:
    explicit Spider(QWidget *parent = nullptr);
    ~Spider();

private slots:
    void on_pushButton_clicked();
    void on_enableButton();
    void fillTreeWidget();

private:
    Ui::Spider *ui;
    SpiderWorker *spiderWorker;
};

#endif // SPIDER_H
