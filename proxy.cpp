#include "proxy.h"

Proxy::Proxy(QObject *parent, Aracne *aracne, unsigned short port) :
    QTcpServer (parent)
{
    this->_aracne = aracne;
    this->_port = port;
    connect(this->_aracne, SIGNAL(turn_on()), this, SLOT(turn_on()));
    connect(this->_aracne, SIGNAL(turn_off()), this, SLOT(turn_off()));
    connect(this->_aracne, SIGNAL(spider_reference(Spider *)), this, SLOT(spider_reference(Spider *)));
    connect(this->_aracne, SIGNAL(dump_reference(Dump *)), this, SLOT(dump_reference(Dump *)));
}

void Proxy::start(){
    if(!listen(QHostAddress::Any, this->_port)){
        qDebug() << "Server could not start";
    } else{
        qDebug() << "Server started on port " << this->_port;
    }
}

void Proxy::turn_on(){
    this->serverOn = true;
}

void Proxy::spider_reference(Spider *spider){
    this->mode = "spider";
    this->_spider = spider;
    connect(this->_spider, SIGNAL(set_mode(QString)), this, SLOT(set_mode(QString)));
}

void Proxy::dump_reference(Dump *dump){
    this->mode = "dump";
    this->_dump = dump;
    connect(this->_dump, SIGNAL(set_mode(QString)), this, SLOT(set_mode(QString)));
}

void Proxy::set_mode(QString mode){
    if(mode != "standard" && mode != "spider" && mode != "dump"){
        qDebug() << "Incorrect mode: " << mode;
        return;
    }
    qDebug() << "Changing mode: " << mode;
    this->mode = mode;
}

void Proxy::turn_off(){
    this->serverOn = false;
}

void Proxy::incomingConnection(qintptr socketDescriptor){
    if(!this->serverOn) return;
    else if(this->mode == "standard") this->standardMode(socketDescriptor);
    else if(this->mode == "spider") this->spiderMode(socketDescriptor);
    else if(this->mode == "dump") this->dumpMode(socketDescriptor);
    else return;
}

void Proxy::standardMode(qintptr socketDescriptor){    
    Client *client = new Client(this, busy, this->clientId++);

    connect(client, SIGNAL(new_request(QByteArray, int)), this->_aracne, SLOT(new_request(QByteArray, int)));
    connect(this->_aracne, SIGNAL(send_reply(QByteArray, int)), client, SLOT(send_reply(QByteArray, int)));
    connect(this->_aracne, SIGNAL(send_request(QByteArray, int)), client, SLOT(send_request(QByteArray, int)));

    client->setAracne(this->_aracne);
    client->setSocket(socketDescriptor);
}

void Proxy::dumpMode(qintptr socketDescriptor){
    qDebug() << "Entering dump Mode";
}

void Proxy::spiderMode(qintptr socketDescriptor){
    this->serverOn = false;
    this->_aracne->setServerOff();
    SpiderWorker *worker = new SpiderWorker(socketDescriptor);

//    connect(client, SIGNAL(new_request(QByteArray, int)), this->_aracne, SLOT(new_request(QByteArray, int)));
//    connect(this->_aracne, SIGNAL(send_reply(QByteArray, int)), client, SLOT(send_reply(QByteArray, int)));
//    connect(this->_aracne, SIGNAL(send_request(QByteArray, int)), client, SLOT(send_request(QByteArray, int)));

    worker->start();
}
