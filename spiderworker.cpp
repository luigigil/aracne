#include "spiderworker.h"
#include <regex>

SpiderWorker::SpiderWorker(qintptr descriptor) {
    this->myDescriptor = descriptor;
}

void SpiderWorker::start(){
    this->setOuterSocket();
    parser = new HttpParser();
}

void SpiderWorker::setOuterSocket(){

    // make a new socket
    this->outerSocket = new QTcpSocket(this);

    qDebug() << "A new socket created!";

    connect(this->outerSocket, SIGNAL(connected()), this, SLOT(connected()));
    connect(this->outerSocket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(this->outerSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    this->outerSocket->setSocketDescriptor(this->myDescriptor);

    qDebug() << " Client connected at " << this->myDescriptor;
}

void SpiderWorker::setInnerSocket(){
    this->innerSocket = new QTcpSocket(this);

    this->parser->parseRequest(this->request);

    char hostname[32];
    strcpy(hostname, this->parser->getHostname().toStdString().c_str());

    qDebug() << Q_FUNC_INFO << "Hostname: " << hostname;
    this->innerSocket->connectToHost(hostname, 80);

    qDebug() << "Wait for connect = " << this->innerSocket->waitForConnected();

    this->innerSocket->write(this->request);

    while(this->innerSocket->waitForReadyRead(3000)){
        while(this->innerSocket->bytesAvailable() > 0){
            this->reply.append(this->innerSocket->readAll());
            this->innerSocket->flush();
        }
    }

    this->innerSocket->disconnectFromHost();
    this->innerSocket->deleteLater();
    if(this->needSendReply)
           this->sendReply();
    this->needSendReply = false;
}

void SpiderWorker::connected(){
    qDebug() << "outerSocket Connected!";
}

void SpiderWorker::disconnected(){
    qDebug() << "Disconnected!";
    this->parseBody();
}

void SpiderWorker::bytesWritten(qint64 bytes){
    qDebug() << "We wrote: " << bytes;
}

void SpiderWorker::sendReply(){
    this->outerSocket->write(this->reply);
    this->outerSocket->disconnectFromHost();
}

void SpiderWorker::readyRead(){
    qDebug() << "Reading...";
    this->request.append(this->outerSocket->readAll());
    this->needSendReply = true;
    this->setInnerSocket();
}

void SpiderWorker::parseBody(){
    std::regex re("href='(.*)'>");
    std::smatch match;
    size_t index = (size_t)reply.indexOf("\r\n\r\n");
    std::string body = reply.toStdString().data() + index + 4;

//    this->listHrefs(body);
}

void SpiderWorker::listHrefs(std::string body){
    size_t begRef, endRef;
    std::string temp = "";
    std::vector<std::string> urlLst;
    std::string path = this->parser->getPath().toStdString().c_str();
    std::string host = this->parser->getHostname().toStdString().c_str();

    begRef = body.find("href=\"");

    while (begRef != std::string::npos) {
        endRef = body.find("\"", begRef + 6);
        if (endRef == std::string::npos) {
            qDebug() << "SPIDER ERROR! While searching href" << endl;
        }

        temp = body.substr(begRef + 6, endRef - (begRef + 6));

        if (temp.find("http://") == std::string::npos) {
            if (temp[0] == '/') {
                urlLst.push_back(path + temp);
            } else {
                urlLst.push_back(temp);
            }
        } else if (temp.find(host) != std::string::npos) {
            // #ifdef MY_DEBUG_SPIDER
            //     cout<< "href com host:\t"<< temp << endl;
            //     cout<<"\t"<< "host length: " host.length() << endl;
            //     cout<<"\t"<< "host: " host << endl;
            // #endif
            urlLst.push_back(temp.substr(temp.find(host) + host.length()));
        }
        begRef = body.find("href=\"", endRef + 1);
    }

//    qDebug() << urlLst;
}
