#ifndef DUMP_H
#define DUMP_H

#include <QDialog>

namespace Ui {
class Dump;
}

class Dump : public QDialog{
    Q_OBJECT

signals:
    void set_mode(QString);

public:
    explicit Dump(QWidget *parent = nullptr);
    ~Dump();

private:
    Ui::Dump *ui;
};

#endif // DUMP_H
