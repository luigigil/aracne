#ifndef PROXY_H
#define PROXY_H

#include "aracne.h"
#include "dump.h"
#include "spider.h"
#include "client.h"
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QAbstractSocket>

class Proxy : public QTcpServer
{
    Q_OBJECT
public:
    Proxy(QObject *parent = nullptr, Aracne *aracne = nullptr, unsigned short port = 8228);
    void start();   

public slots:
    void turn_on();
    void turn_off();
    void set_mode(QString);
    void spider_reference(Spider *spider);
    void dump_reference(Dump *dump);

private:    
    unsigned short _port;
    Aracne *_aracne;
    Dump *_dump;
    Spider *_spider;
    bool busy = false;
    int clientId = 0;
    bool serverOn = false;
    QString mode = "standard";
    void standardMode(qintptr socketDescriptor);
    void dumpMode(qintptr socketDescriptor);
    void spiderMode(qintptr socketDescriptor);

protected:
    void incomingConnection(qintptr socketDescriptor) Q_DECL_OVERRIDE;

};

#endif // PROXY_H
