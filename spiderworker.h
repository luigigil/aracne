#ifndef SPIDERWORKER_H
#define SPIDERWORKER_H

//#include "aracne.h"
#include <QtCore>
#include <QtNetwork>
#include <QByteArray>
#include "httpparser.h"

class SpiderWorker : public QObject
{
    Q_OBJECT
public:
    explicit SpiderWorker(qintptr);
    void start();

signals:
    void new_reply(QByteArray, int);
    void fillTreeWidget();

public slots:
    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();

private:
    QTcpSocket *innerSocket;
    QTcpSocket *outerSocket;
    qintptr myDescriptor;
    QByteArray request;
    QByteArray url;
    QByteArray reply;
    HttpParser *parser;
    void setInnerSocket();
    void setOuterSocket();
    void sendReply();
    void parseBody();
    void listHrefs(std::string);
    bool needSendReply = false;

};

#endif // SPIDERWORKER_H
