#include "dump.h"
#include "ui_dump.h"

Dump::Dump(QWidget *parent) : QDialog(parent), ui(new Ui::Dump){
    ui->setupUi(this);
}

Dump::~Dump(){
    emit set_mode("standard");
    delete ui;
}
