#include "spider.h"
#include "ui_spider.h"

Spider::Spider(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Spider){
    ui->setupUi(this);
}

Spider::~Spider(){
    emit set_mode("standard");
    delete ui;
}

void Spider::on_pushButton_clicked(){

}

void Spider::fillTreeWidget(){

}

void Spider::on_enableButton(){
    this->ui->pushButton->setDisabled(false);
}
