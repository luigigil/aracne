#ifndef SPIDERCLIENT_H
#define SPIDERCLIENT_H


#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include <QThreadPool>
#include "socket.h"
#include "aracne.h"

class SpiderClient : public QObject
{
    Q_OBJECT
public:
    explicit SpiderClient(QObject *parent = nullptr, bool busy = false, int id = 0);
    void setSocket(qintptr Descriptor);
    void setAracne(Aracne *aracne);

signals:
    void new_request(QByteArray, int);

public slots:
    void connected();
    void disconnected();
    void readyRead();
    void send_request(QByteArray, int);
    // make the server fully ascynchronous
    // by doing time consuming task
    void send_reply(QByteArray, int);

private:
    QTcpSocket *socket;
    bool busy;
    int myId;
    Aracne *aracne;

};

#endif // SPIDERCLIENT_H
