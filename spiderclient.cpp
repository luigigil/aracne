#include "spiderclient.h"

SpiderClient::SpiderClient(QObject *parent, bool busy, int id) :
    QObject(parent)
{
    this->busy = busy;
    this->myId = id;
}

void SpiderClient::setSocket(qintptr Descriptor)
{
    // make a new socket
    socket = new QTcpSocket(this);

    qDebug() << "A new socket created!";

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    socket->setSocketDescriptor(Descriptor);

    qDebug() << " SpiderClient connected at " << Descriptor;
}

void SpiderClient::setAracne(Aracne *aracne){
    this->aracne = aracne;
}

// asynchronous - runs separately from the thread we created
void SpiderClient::connected()
{
    qDebug() << "SpiderClient connected event";
}

// asynchronous
void SpiderClient::disconnected()
{
    qDebug() << "SpiderClient disconnected";
}

// Our main thread of execution
// This happening via main thread
// Our code running in our thread not in Qthread
// That's why we're getting the thread from the pool

void SpiderClient::readyRead()
{
    qDebug() << Q_FUNC_INFO << "Socket::readyRead()";

    QByteArray request = socket->readAll();

    qDebug() << Q_FUNC_INFO << request;

    emit new_request(request, this->myId);
}

void SpiderClient::send_request(QByteArray request, int clientId){
    if(this->myId == clientId){
        Socket *socket = new Socket(request, clientId);

        connect(socket, SIGNAL(new_reply(QByteArray, int)), this->aracne, SLOT(new_reply(QByteArray, int)));

        socket->start();
//        qDebug() << "Starting a new task using a thread from the QThreadPool";
//        QThreadPool::globalInstance()->start(socket);
    }
}

// After a task performed a time consuming task.
// We grab the result here, and send it to client
void SpiderClient::send_reply(QByteArray reply, int clientId){
    if(clientId == this->myId){
        socket->write(reply);
        socket->disconnectFromHost();
    }
}
